import React from 'react';


class Person extends React.Component {

    state = {
        firstName : "Puneet"
    }

    onChange =(e) => {
            this.setState({firstName : e.target.value})
    }

    onClick = ()=>{
        this.label.textContent = "Updated";
    }
    render(){
        
        const {firstName} = this.state;
        return <div>
            <label ref={node=> this.label = node}>First Name : </label>
            <input type = "text" value = {firstName} onChange= {this.onChange}/>
            <button onClick = {this.onClick}> Click me </button>

        </div>
    }
}

export default Person;
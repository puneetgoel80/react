import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Clock from './clock';
import Person from './person';

class App extends Component {
  
  state ={
    now : new Date()
  };
  
 
  componentDidMount() {
    this.handle = setInterval(()=> {this.setState({now : new Date()})});
  };

componentWillUnmount(){
clearInterval(this.handle);
}

  render() {
    return (
      <div className="App">
       
        <Clock now={this.state.now}/>
        <Person/>

      </div>
    );
  }
}

export default App;

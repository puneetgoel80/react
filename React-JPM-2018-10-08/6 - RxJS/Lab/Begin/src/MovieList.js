import React, { Component } from "react";
import MovieRow from "./MovieRow";
import {ajax } from 'rxjs/ajax';
import { dispatch } from "rxjs/internal/observable/range";

class MovieList extends Component {
  state = {
    movies: []
  };

  componentDidMount() {
    ajax.getJSON("/api/movies").subscribe(movies=> this.setState({movies : movies}));
  }

  render() {
    const rows = this.state.movies.map(movie => (
      <MovieRow key={movie.id} movie={movie} />
    ));

    return (
      <table className="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Title</th>
            <th />
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </table>
    );
  }
}

export default MovieList;

import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import MovieList from './movieList' 
import MovieEdit from './movieEdit' 

class App extends Component {
  state = {
    id : '',
    editMode : false
}
 
editMovie = (moveId,mode)=>{
  this.setState({id:moveId, editMode : mode});
}

  render() {
    return (
      <div className="container">
       <h1>First Application</h1>
     
     { !this.state.editMode ? 
        <MovieList editMovie={this.editMovie} />:<MovieEdit editMovie={this.editMovie} movieId={this.state.id}/>
      }
      </div>
    );
  }
}

export default App;

import React  from 'react';

class MovieRow extends React.Component {

   
   
clicked = ()=> {
    const{movie,editMovie} = this.props;
    editMovie(movie.id,true);
}

    render(){
        const{movie,editMovie} = this.props;

        return <tr>
            <td>{movie.title}</td>
            <td>{movie.director}</td>
            <td><button onClick ={this.clicked}>Edit</button></td>
        </tr>
    }
   
}
export default MovieRow;
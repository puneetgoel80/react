import React  from 'react';
import MovieRow from './movieRow'

class MovieList extends React.Component {

    state =  {
      movieList : []
    }

    componentDidMount(){
            fetch('/api/movies')
            .then(resp => resp.json())
            .then(json => this.setState({movieList : json}));
    }
    render(){
      const editMovie = this.props.editMovie;
      return <table className="table table-bordered table-striped">
       <tbody>
       <tr>
        <td>Title</td>
        <td>Directors</td>
        <td>Edit ?</td>
        </tr>

        {this.state.movieList.map(movie=> <MovieRow key ={movie.id} movie={movie} editMovie = {editMovie} />)}
    </tbody>
        </table>
    }
}
export default MovieList;
import Calculator from './calculator';

export default function wireup(calculator: Calculator) {
  const inpX = $('#inpX');
  const inpY = $('#inpY');
  const preResult = $('#preResult');

  $('#btnAdd').click(() => {
    const result = calculator.add(inpX.val() as number, inpY.val() as number);
    preResult.text(result);
  });

  $('#btnSubtract').click(() => {
    const result = calculator.subtract(
      inpX.val() as number,
      inpY.val() as number
    );
    preResult.text(result);
  });

  $('#btnMultiply').click(() => {
    const result = calculator.multiply(
      inpX.val() as number,
      inpY.val() as number
    );
    preResult.text(result);
  });

  $('#btnDivide').click(() => {
    const result = calculator.divide(
      inpX.val() as number,
      inpY.val() as number
    );
    preResult.text(result);
  });
}

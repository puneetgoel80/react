import React, { Component } from "react";

import "bootstrap/dist/css/bootstrap.css";

import MovieList from "./MovieList";
import MovieEdit from "./MovieEdit";

import {BrowserRouter, Switch,Route,Redirect} from 'react-router-dom';

class App extends Component {
  state = {
    editMode: false,
    id: undefined
  };

  toEditMode = e => {
    console.log("toEditMode", e.id);
    this.setState({
      editMode: true,
      id: e.id
    });
  };
  
  toListMode = () => {
    console.log("toListMode");
    this.setState({
      editMode: false,
      id: undefined
    });
  };

  render() {
    var component;

    if (this.state.editMode) {
      component = (
        <MovieEdit toListMode={this.toListMode} movieId={this.state.id} />
      );
    } else {
      component = <MovieList toEditMode={this.toEditMode} />;
    }

    return (
     <BrowserRouter>
     <div className="container">
        <h1>React-Router</h1>
       <Switch>
        <Route path="/movies" component={MovieList}/>
        <Route path="/movie/:id" component={MovieEdit}/>
        <Redirect to="/movies"/>
        </Switch>
      </div>
      </BrowserRouter>
    );
  }
}

export default App;

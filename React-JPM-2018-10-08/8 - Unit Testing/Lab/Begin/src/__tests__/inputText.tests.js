import React from "react"; 
import { mount } from "enzyme";
import InputText from "../InputText";

describe("The InputText", () => { 
      it("renders a input", () => {

      const component = mount(<InputText>Label</InputText>); 
      const inputs = component.find("input");
       expect(inputs.length).toBe(1);
     }
    ); 
    }
);